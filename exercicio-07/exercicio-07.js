/*
É um triângulo ou não é? Você deverá criar um programa que decida para o usuário se as
três entradas de valores numéricos oferecidas por ele correspondem aos lados de triângulo
ou não.
*/

var lado1, lado2, lado3;
lado1 = 3;
lado2 = 2;
lado3 = 4;

if (lado1 > Math.abs((lado2 - lado3)) && lado1 < (lado2 + lado3) &&	
	lado3 > Math.abs((lado1 - lado2)) && lado3 < (lado2 + lado1) && 
	lado2 > Math.abs((lado1 - lado3)) && lado2 < (lado1 + lado3)) {
	console.log("Os valores informados: '" + lado1 + "/" + lado2 + "/" + lado3 + "' correspondem a um triângulo!");
} else {
	console.log("Os valores informados: '" + lado1 + "/" + lado2 + "/" + lado3 + "' Não correspondem a um triângulo!");
}