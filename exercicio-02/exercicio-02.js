/*
Dado dois valores numéricos de entrada, realize a soma, a subtração, a multiplicação e a
divisão entre eles. Cada uma das operações deverá ser mostrada no console.
*/

var num1, num2, total;
num1 = 3;
num2 = 3;

console.log("Para os valores de entrada '" + num1 + "' e '" + num2 + "' temos que:");
total = num1 + num2;
console.log(num1 + " + " + num2 + " = " + total);
total = num1 - num2;
console.log(num1 + " - " + num2 + " = " + total);
total = num1 * num2;
console.log(num1 + " * " + num2 + " = " + total);
total = num1 / num2;
console.log(num1 + " / " + num2 + " = " + total);
