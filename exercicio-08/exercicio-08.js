/*
Vamos criar um jogo ao estilo Role Playing Game! Começaremos por definir o sistema
de batalha e, para isso, você precisará desenvolver um programa com quatro entradas do
tipo string e duas entradas do tipo numérico. Duas entradas (string) guardarão o nome
dos adversários e as outras duas (string) suas respectivas armas. As entradas do tipo
numérico irão guardar valores entre 1 e 100 representando os pontos de força de cada
adversário. O jogo consistirá de apenas um turno e os adversário possuem 1800 pontos
de vida cada. Ao executar o turno, um valor pseudoaleatório entre 1 e 20 deverá ser
gerado. Este valor servirá como potencializador da força de ataque dos adversários. Os
dois adversários irão atacar ao mesmo tempo no mesmo turno, os seus respectivos valores
de força de ataque deverão se multiplicar ao fator potencializador aleatório somado ao
fator de ataque da arma. O valor total deverá ser subtraı́do pelo fator de defesa da arma
vezes 100.
*/

let adversario1, adversario2, arma_Adv1, arma_Adv2;
var forca_Adv1, forca_Adv2, pontos_Adv1, pontos_Adv2;
const vida = 1800;

console.log("Jogo ao estilo Role Playing Game!");
console.log("\nArma\t\t\tPontos de Ataque Pontos de Defesa");
console.log("Martelo de Madeira\t\t16\t\t2");
console.log("Florete\t\t\t\t10\t\t7");
console.log("Arco e Flechas\t\t\t20\t\t0");
console.log("Varinha de Inverno\t\t17\t\t1\n");

/* gerar valor aleatório com a função matemática 'random' e limita de 1-20 com 'floor'*/
var vlr_aleatorio = Math.floor(Math.random() * 20 + 1);

adversario1 = "Bardo Gerdi";
arma_Adv1 = ['Arco e Flechas', 20, 0];
forca_Adv1 = 50;

adversario2 = "Cavaleiro Torium";
arma_Adv2 = ['Martelo de Madeira', 16, 2];
forca_Adv2 = 80;

/*Mostra o ataque*/
console.log("\nAtaque: " + adversario1 + " - " + arma_Adv1[0] + " - Força: " + forca_Adv1);
console.log("Ataque: " + adversario2 + " - " + arma_Adv2[0] + " - Força: " + forca_Adv2 + ".\n\n");

/*Torna o resultado absoluto com a função matemática 'abs' */
pontos_Adv1 = Math.abs(forca_Adv1 * vlr_aleatorio + arma_Adv1[1] - (arma_Adv1[2] * 100));
pontos_Adv2 = Math.abs(forca_Adv2 * vlr_aleatorio + arma_Adv2[1] - (arma_Adv2[2] * 100));

console.log(adversario1 + " perdeu " + pontos_Adv2 + " pontos de vida no ataque de " + 
	adversario2 + ". ");
console.log(adversario2 + " perdeu "+ pontos_Adv1 + " pontos de vida no ataque de " + 
	adversario1 + ". ");

/*Classifica o resultado*/
if (pontos_Adv1 <= vida && pontos_Adv2 <= vida) {
	console.log("Todos sobreviveram.");
} else if (pontos_Adv1 > vida && pontos_Adv2 > vida) {
	console.log("Todos morreram.");
} else if (pontos_Adv1 > vida) {
	console.log(adversario2 + " morreu.");
} else {
	console.log(adversario1 + " morreu.");
}
