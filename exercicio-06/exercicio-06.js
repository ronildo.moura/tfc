/*
Crie um programa que decida se um aluno foi aprovado ou não. Caso não seja aprovado,
o programa deverá mostrar quantos pontos o aluno precisaria para completar a média de
60 pontos. O programa tem quatro entradas que representam a pontuação do aluno em
quatro atividades elaboradas pelo professor. A primeira é de 15 pontos, a segunda de
25 pontos, a terceira de 25 pontos e a quarta de 35 pontos. O lançamento de notas não
pode ultrapassar a pontuação máxima de cada atividade.
*/

const notaMinima = 60;
var atividade, media, saldo;
atividade = [15, 25, 25, 35];
media = atividade[0] + atividade[1] + atividade[2] + atividade[3];

if (atividade[0] > 15) {
	console.log("Erro: atividade 1 não pode ter mais que 15 pontos lançados!");
} else if (atividade[1] > 25) {
	console.log("Erro: atividade 2 não pode ter mais que 25 pontos lançados!");
} else if (atividade[2] > 25 ) {
	console.log("Erro: atividade 3 não pode ter mais que 25 pontos lançados!");
} else if (atividade[3] > 35) {
	console.log("Erro: atividade 4 não pode ter mais que 35 pontos lançados!");
} else {
	if (media >= notaMinima) {
		console.log("O aluno foi aprovado!");
	} else {
		saldo = notaMinima - media;	
		console.log("O aluno foi reprovado, faltando " + saldo + " pontos para a pontuação mı́nima de aprovação!");
	}
}
