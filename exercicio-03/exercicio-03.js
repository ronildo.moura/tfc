/*
Realize o cadastro de uma pessoa como se fosse uma empresa multinacional sequestradora
de informações sensı́veis de usuário! O programa terá cinco entradas: nome, idade, CPF,
e-mail e telefone.
*/

var nome, idade, cpf, email, telefone;
nome = "Fulano da Silva";
idade = 20;
cpf = "111.875.166-89";
email = "fulanodasilva@gmail.com";
telefone = "(34) 9 8754 1144";

console.log("Que bom que você decidiu compartilhar seus dados pessoais, " + nome + ". Vejo aqui que você tem " + idade +" anos de idade e seu CPF é " + cpf +". Nossa rede de 3498 lojas entrarão em contato com você pelo telefone " + telefone + " para vender inúmeros produtos de qualidade e que certamente você deseja ter em sua casa. Deseja receber mais informações promocionais no seu e-mail “"+ email + "”? Para que perguntar, não é? Vou enviar mesmo assim.");