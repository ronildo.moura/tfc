/*
Ensine aos estadounidenses que o certo mesmo é graus Celsius!!! Faça um conversor de
Fahrenheit para Celsius. Dica quente!
*/

var grausC, grausF;
grausF = 89.5;
grausC = (grausF - 32) / 9 * 5;

console.log("Conversor de Fahrenheit para Celsius");
console.log("Dica quente: \nDe " + grausF + " graus Fahrenheit temos " + grausC.toFixed(2) + " graus Celsius!");