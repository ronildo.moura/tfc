/*
Na sala de aula de uma disciplina do IFTM há apenas três pessoas que optaram pelo pres-
encial no ensino hı́brido. O professor decidiu mostrar o aproveitamento das atividades
presenciais a partir do valor da média da nota final destes.
*/

var aluno1, aluno2, aluno3, media;
aluno1 = 38.5;
aluno2 = 86.4;
aluno3 = 60.2;
media = (aluno1 + aluno2 + aluno3) / 3;

console.log("O aluno 1 teve um aproveitamento de: " + aluno1);
console.log("O aluno 2 teve um aproveitamento de: " + aluno2);
console.log("O aluno 3 teve um aproveitamento de: " + aluno3);
console.log("A média da nota final de todos os alunos que frequentaram o presencial foi de: " + media.toFixed(2));